# Chef-types

## Description

Chef-Types is a library part of the Chef library that provides tools and
functionalities for dealing with types.

The goal is to provide the programmer with a reusable and solid way to create
new types and manipulate existing ones.

This library integrates with other Chef libraries, but has no dependency on
that and can be used as a stand-alone component.

## Getting started

The library is header only, so you may download the files and place them in
your include path.

If you are using cmake, you may use the following code to make ChefFun available
to your project:

```cmake
cmake_minimum_required(VERSION 3.14)

#...

include( FetchContent )

FetchContent_Declare(
    chef-types
    GIT_REPOSITORY https://gitlab.com/libchef/chef-types.git
    GIT_TAG        1.0.0
)

FetchContent_MakeAvailable( chef-fsm )

target_include_directories(
    YourTarget
    PUBLIC
        ${chef-types_SOURCE_DIR}/include
        # other include paths
)
```

## Support
The code is provided as is, there is no granted support.
You may contact me via email, but I cannot guarantee any answer.

## Roadmap
Support for arithmetic refined types.

## Contributing
Users are welcome, contributions even more. Forking and proposing MR are the
preferred way to make contributions. Feel free to contact the author.

Authors and acknowledgment
see [AUTHORS] file.

License
Apache License v2.0

## Project status

Thise project is a proof of concept to demonstrate that it is possible to
enforce contracts in C++ using types instead of assertions. Although the code
is simple and easy to review, it has never been used in a production software.
