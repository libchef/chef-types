/**
 * @author 
 * @date 07/03/24.
 */

#include <gtest/gtest.h>
#include <ChefTypes/Ptr.hh>

using namespace ChefTypes;

TEST( ChefType_test, construction )
{
    auto ptr = ChefTypes::Ptr<int>::make( nullptr );
    ASSERT_FALSE( ptr.has_value() );

    int i = 42;
    ptr = ChefTypes::Ptr<int>::make( &i );
    ASSERT_TRUE( ptr.has_value() );
}

Ptr<int> f1( Ptr<int> a )
{
    return a;
}

Ptr<const int> f2( Ptr<const int> a )
{
    return a;
}

Ptr<const int> f3( Ptr<int> a )
{
    return a;
}


TEST( ChefType_test, function_call )
{
    int i=42;
    auto p1 = Ptr<int>::make( &i ).value();
    auto r1 = f1( p1 );
    ASSERT_TRUE( r1 == &i );

    auto p2 = Ptr<int>::make( &i ).value();
    auto r2 = f2( p2 );
    ASSERT_TRUE( r2 == &i );

    auto p3 = Ptr<int>::make( &i ).value();
    auto r3 = f3( p3 );
    ASSERT_TRUE( r3 == &i );
}

class Base {};
class Derived : public Base {};

void f4( Ptr<Base> b )
{
    (void)b;
}

TEST( ChefType_test, liskov )
{
    auto a = Derived{};
    auto pa = Ptr<Derived>::make( &a ).value();
    f4( pa );
    Ptr<Base> pb = pa;
    ASSERT_EQ( pb, pa );
}

TEST( ChefType_test, dereference )
{
    int n = 1337;
    auto pn = Ptr<int>::make( &n ).value();
    ASSERT_EQ( *pn, n );

    struct Foo {
        int a;
        std::string b;
    };
    Foo foo { 42, "answer" };
    auto p = Ptr<Foo>::make(&foo).value();
    ASSERT_EQ( p->a, foo.a );
    ASSERT_EQ( p->b, foo.b );
}
