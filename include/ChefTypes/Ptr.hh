/**
 * @author Massimiliano Pagani
 * @date 2024-03-06.
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#if !defined(CHEF_TYPE_PTR_HH)
#define CHEF_TYPE_PTR_HH

#include <optional>
#include <type_traits>

namespace ChefTypes
{
    template<typename T>
    class Ptr
    {
    public:
        /**
         * Factory method. Use this static method to create new instances of
         * Ptr<T>.
         *
         * @param p a pointer
         * @return a std::optional containing a pointer to *p if p is not
         *         nullptr. An empty std::optional otherwise.
         */
        static constexpr std::optional<Ptr<T>> make( T* p ) noexcept;

        /**
         * Dereference operator. You can use this operator to access the object
         * pointed by the pointer.
         *
         * @return the pointer to the pointed object.
         */
        constexpr T* operator->() const noexcept;

        /**
         * Dereference operator. You can use this operator to access the object
         * pointed by the pointer.
         *
         * @return a reference to the pointed object.
         */
        constexpr T& operator*() const noexcept;

        /**
         * Get the pointer. This method returns the encapsulated pointer
         *
         * @return the pointer. This pointer will never be a nullptr.
         */
        constexpr T* get() const noexcept;

        /**
         * Copy constructor. This constructor creates a new instance of Ptr<T>
         * from a Ptr<U> provided that T and U are not the same type and you
         * can assign a pointer to U to a pointer to T (e.g. when T is a
         * superclass of U).
         *
         * @param p the Ptr<U> you want to construct this Ptr<T> from.
         */
        template<
            typename U,
            typename E=
                    std::enable_if<
                        std::conjunction_v<
                            std::is_convertible<U*,T*>,
                            std::negation<std::is_same<T,U>>
                        >
                    >
        >
        constexpr Ptr( Ptr<U> p ) : mP{ p.get() } {}

        /**
         * Pointer arithmetic. The difference between two Ptr<T> gives you the
         * count of Ts between the two pointers.
         *
         * Adding or subtracting a number to a Ptr<T> gives you an optional
         * Ptr<T>. If the result of the operation is a valid pointer, the
         * optional will contain the new Ptr<T>. Otherwise, the optional will
         * be empty.
         *
         * @param p the T* you want to construct this Ptr<T> from.
         * @param d the number you want to add or subtract to the pointer.
         * @{
         */
        constexpr ptrdiff_t operator-( Ptr<T> p ) const noexcept;
        constexpr std::optional<Ptr<T>> operator+( ptrdiff_t d ) const noexcept;
        constexpr std::optional<Ptr<T>> operator-( ptrdiff_t d ) const noexcept;
        /** @} */

        // void* conversion
        /**
         * Void* conversion operator. This operator allows you to convert a
         * Ptr<T> to a void*.
         *
         * @tparam E a dummy parameter to enable the operator according to the
         *           const-ness of T.
         * @return a void* pointer to the same memory location as the Ptr<T>.
         *         This pointer will never be nullptr.
         * @{
         */
        template<typename E=std::enable_if<std::negation_v<std::is_const<T>>>>
        operator void* () const noexcept { return mP; }
        template<typename E=std::enable_if<std::is_const_v<T>>>
        operator void const* () const noexcept { return mP; }
        /** @} */

        /**
         * Reinterpret as operator. This operator allows you to reinterpret a
         * Ptr<T> as a Ptr<U>. This is useful when you have a pointer to a
         * superclass and you want to treat it as a pointer to a subclass.
         *
         * @tparam U the target type.
         * @return a Ptr<U> pointing to the same memory location as the Ptr<T>.
         *
         */
        template<typename U>
        Ptr<U> reinterpretAs() const noexcept { return Ptr( reinterpret_cast<U>(mP)); }

    private:
        explicit constexpr  Ptr( T* p ) noexcept;

        T* mP;
    };

    /**
     * Equality operators. These operators allow you to compare two Ptr<T> for
     * equality.
     *
     * @tparam T left-hand side type.
     * @tparam U right-hand side type.
     * @param a left-hand side operand.
     * @param b right-hand side operand.
     * @return true if the two pointers point to the same memory location.
     * @{
     */
    template<typename T,typename U>
    constexpr bool operator==( Ptr<T> a, Ptr<U> b ) noexcept;

    template<typename T,typename U>
    constexpr bool operator==( Ptr<T> a, U* b ) noexcept;

    template<typename T,typename U>
    constexpr bool operator==( T* a, Ptr<U> b ) noexcept;

    /** @} */

    // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // ::::::::::::::::::::::::::::::::::::::::::::::::::: implementation :::
    // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    template<typename T>
    inline constexpr Ptr<T>::Ptr( T* p ) noexcept
            : mP( p )
    {
    }

    template<typename T>
    constexpr std::optional<Ptr<T>>
    Ptr<T>::make( T* p ) noexcept
    {
        return p != nullptr ? std::optional( Ptr(p) ) : std::nullopt;
    }

    template<typename T>
    constexpr T* Ptr<T>::get() const noexcept
    {
        return mP;
    }

    template<typename T>
    constexpr T* Ptr<T>::operator->() const noexcept
    {
        return mP;
    }
    
    template<typename T>
    constexpr T& Ptr<T>::operator*() const noexcept
    {
        return *mP;
    }

    template<typename T,typename U>
    constexpr bool operator==( Ptr<T> a, Ptr<U> b ) noexcept
    {
        return a.get() == b.get();
    }

    template<typename T,typename U>
    constexpr bool operator==( Ptr<T> a, U* b ) noexcept
    {
        return a.get() == b;
    }

    template<typename T,typename U>
    constexpr bool operator==( T* a, Ptr<U> b ) noexcept
    {
        return a == b.get();
    }

    template<typename T>
    constexpr ptrdiff_t Ptr<T>::operator-( Ptr<T> p ) const noexcept
    {
        return mP - p.mP;
    }

    template<typename T>
    constexpr std::optional<Ptr<T>> Ptr<T>::operator+( ptrdiff_t d ) const noexcept
    {
        return make( mP + d );
    }

    template<typename T>
    constexpr std::optional<Ptr<T>> Ptr<T>::operator-( ptrdiff_t d ) const noexcept
    {
        return make( mP - d );
    }
}

#endif //CHEF_TYPE_PTR_HH
